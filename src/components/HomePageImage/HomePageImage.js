import React, { Component } from 'react'
import { Image } from 'semantic-ui-react'

class HomePageImage extends Component {
  render() {
    return <div>
    <Image 
      size='mini' 
      src='https://image.businessinsider.com/558abc7669bedd3e7d57802e?width=1300&format=jpeg&auto=webp' 
      style={{ 
        width: '100%', 
        display: 'block', 
        marginLeft: 'auto', 
        marginRight: 'auto' 
      }}
    />
    </div>
  }
}
export default HomePageImage