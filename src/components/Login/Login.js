import React, { Component } from 'react'
import MatrixClient from '@caelum-tech/lorena-matrix-client'
import { Button, Divider, Form, Grid, Modal, Segment, Header } from 'semantic-ui-react'
import Zen from '@caelum-tech/zenroom-lib'
var QRCode = require('qrcode.react')

class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      client: undefined,
      credentialRequest: undefined,
      guest: undefined,
      modalOpen: false,
    }
  }

  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => this.setState({ modalOpen: false })

  // Called when the website receives a credential via matrix
  async authenticationCallback (event, data) {
    const body = event.content.body
    try {
      const newCredential = JSON.parse(body)
      console.log(newCredential)
      const cs0 = newCredential[0].claims[0].credentialSubject[0]
      // TODO: check credential

      let z = new Zen()
      // TODO: add --> newCredential[0].nonRevocationProof[1].zenroom.draft,
      let signedCredential = await z.checkSignature(
        'Alice',
        {
          Alice: {
            public_key: newCredential[0].nonRevocationProof[1].verificationMethod
          }
        },
        {
          zenroom: { curve: 'goldilocks', encoding: 'url64', version: '1.0.0+53387e8', scenario: 'simple' },
          Alice: newCredential[0].nonRevocationProof[1].zenroom
        },
        'Bob'
      )
      console.log(signedCredential)
      if (signedCredential.signature !== 'correct' ) {
        console.log('Message verification: Credential not valid')
        throw new Error('Message verification: Credential not valid')
      }
      // Put userName in the banner
      this.props.onLogin(`${cs0.first} ${cs0.last}`)
      // close dialog
      this.handleClose()
    } catch (e) {
      console.log(e)
    }
  }

  async componentDidMount() {
    const client = new MatrixClient()
    this.setState({client})
    const matrixServerUrl = process.env.REACT_APP_MATRIX_SERVER_URL
    const guest = await client.registerForAuthenticationAsGuest(matrixServerUrl, (event, data) => {this.authenticationCallback(event,data)})
    this.setState({guest})
    if (guest) {
      const newCR = client.getUrlCodifiedCredentialRequest('CaelumEmailCredential', 'did:lor:hoverfloat', process.env.REACT_APP_LORENA_URL, 'json')
      this.setState({credentialRequest: newCR})
    }
  }

  render() {
    let qrCode
    if (this.state.credentialRequest) {
      qrCode =
        <QRCode
          value={this.state.credentialRequest}
          size={256}
        />
    } else {
      qrCode = null
    }
    return (
      <Modal trigger={<Button onClick={this.handleOpen}>Login/Register</Button>} closeIcon centered={true} open={this.state.modalOpen} onClose={this.handleClose}>
        <Modal.Content >
          <Segment placeholder>
            <Grid columns={2} relaxed='very' stackable>
              <Divider vertical>Or</Divider>
              <Grid.Column>
                <Form>
                  <Form.Input
                    icon='user'
                    iconPosition='left'
                    label='Username'
                    placeholder='Username'
                  />
                  <Form.Input
                    icon='lock'
                    iconPosition='left'
                    label='Password'
                    type='password'
                  />
                  <Button content='Login' primary />
                </Form>
              </Grid.Column>

              <Grid.Column verticalAlign='middle'>
                <Header as='h2' icon textAlign='center'>
                  <Header.Content>Register using Lorena<br/>Scan Here</Header.Content>
                </Header>
                <div style={{ display: 'table', margin: '0 auto', marginTop: '2ex'}}>
                  { qrCode }
                </div>
              </Grid.Column>
            </Grid>
          </Segment>
        </Modal.Content>
      </Modal>
    )
  }
}
export default Login
