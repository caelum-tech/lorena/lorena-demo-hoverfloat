import React, { Component } from 'react'
import 'semantic-ui-css/semantic.min.css'
import './App.css'

import HoverFloatHeader from '../header/HoverFloatHeader'
import { HomePageImage } from '../../components'

class App extends Component {
  render() {
    return (
      <div className="App">
        <HoverFloatHeader />
        <HomePageImage />
      </div>
    )
  }
}

export default App