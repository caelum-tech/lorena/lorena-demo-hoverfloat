import React, { Component } from 'react'
import { Button, Container, Dropdown, Image, Menu, MenuItem } from 'semantic-ui-react'
import Logo from '../../assets/logo.png'
import { Login } from '../../components'
import './HoverFloatHeader.css'

class HoverFloatHeader extends Component {
  constructor(props){
    super(props)
    this.state = {
      userName: undefined,
    }
  }

  handleLogin = (userName) => {
    this.setState({userName})
  }

  handleLogout = () => {
    this.setState({userName: undefined})
  }

  render() {
    const logButton = (this.state.userName === undefined) ?
      <Login onLogin={this.handleLogin}/>
      : <Button secondary onClick={this.handleLogout}>Log out</Button>

    return <Container>
    <Menu fixed='top' inverted>
        <Container>
        <Menu.Item as='a' header>
          <Image size='mini' src={Logo} style={{ marginRight: '1.5em' }} />
            HoverFloat
        </Menu.Item>
        <Menu.Item as='a'>Home</Menu.Item>
        <Dropdown item simple text='Boards'>
            <Dropdown.Menu>
            <Dropdown.Item>Smooth</Dropdown.Item>
            <Dropdown.Item>Fast</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Header>Offers</Dropdown.Header>
            <Dropdown.Item >Monthly Fee</Dropdown.Item>
            <Dropdown.Item>Yearly Fee</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
        <Menu.Item header position='right'>
          <div align='right'>{this.state.userName}</div>
        </Menu.Item>
        <Menu.Menu position='right'>
          <MenuItem >
            { logButton }
          </MenuItem>
        </Menu.Menu>
        </Container>
    </Menu>
    </Container>
  }
}
export default HoverFloatHeader