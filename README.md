# HoverFloat

[HoverFloat](https://hoverfloat.com) is a ficticious service for renting hoverboards (which sadly don't exist).  It is used to demonstrate the [Lorena](https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/intro.md) self-sovereign identity platform.

## Quick start
```bash
git clone https://gitlab.com/caelum-tech/caelum-verifier-3rdparty.git
cd caelum-verifier-3rdparty/hoverfloat
yarn install  
./node_modules/@caelum-tech/zenroom-lib/bin/zenroom_modules.sh
yarn start
```

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
